[//]: # Nová anketa - 2. etapa

## Zhodnocení pilotního testování 1/3

* V souladu s plánem bylo testováno pouze rozhraní pro vyplnění anketních lístků.
* Proběhlo s mírným zpožděním (konec ledna - březen)
* Zúčastnily se všechny fakulty (FEL staré rozhraní, FS a FD obě)
* Pouze předmětová anketa (nebyly fakultní otázky)
* Všichni měli stejné otázky.

## Zhodnocení pilotního testování 2/3

### vyplněnost po fakultách

\includegraphics[width=55mm]{./images/pilotni-testovani-vyplnenost.png}

* FEL (staré rozhraní)
* 4 873 vyplněných lístků ze 16 871; vyplněnost 29%

### nepředmětová anketa o novém rozhraní
\includegraphics[width=25mm]{./images/hodnoceni-noveho-rozhrani.png}

* celkem 539 odpovědí, 157 textových komentářů
* výsledná známka 1.97

## Zhodnocení pilotního testování 3/3

* testování považuji za úspěšné
* koncepci můžeme zachovat
* včetně "pouze" role "vyučující"
* student může hodnotit více vyučujících v předmětu

### požadavky na doplnění

* fakultní otázky (nepředmětové)
* vynucení textových odpovědí u špatných hodnocení
* přístup pro ukončené studenty (po státnicích)
* sběr požadvků stále probíhá
* https://gitlab.fit.cvut.cz/anketa_cvut/nova-anketa-doc/issues

## Prezentace výsledků

* u pilotního testování použit starý způsob prezentace výsledků

### v čem nevyhovuje

* nevhodný proces zpracování
* kopírování dat z jednoho systému do druhého
* jiné rozhraní
* dlouho trvá než se výsledky dostanou ke studentům
* výsledky nejsou na jednom místě

## Fáze zpracování výsledků

* F1: příprava ankety
* F2: otevření ankety
* F3: uzavření ankety
* F4: cenzura + případný nesouhlas vyčujících se zveřejněním komentářů
* F5: komentáře učitelů
* F6: zobrazení výsledků studentům

### v novém rozhraní

* fakulty budou fáze (i nadále) řidit podle svého harmonogramu
* fáze F1, F2, F3, F4 zachovat
* fáze F5 a F6 mohou probíhat současně
* výsledky budou ve stejné aplikaci jako vyplňování
* lepší zobrazení (vyhledávání, podpora pro vedoucí kateder, ...)
* budou moci komentovat i vedoucí kateder
* historické trendy (omezeně) - dále pohledy nad datovým skladem

## konkrétní plán 2. fáze

* přejít celkově na novou anketu
* příprava ankety za semestr B182
* možnost nepředmětových otázek
* možnost modifikace předmětových otázek pro jednotlivé fakulty
* prozatím budeme řešit mailem se správci a přímou úpravou v databázi
* od května bude možné jednotlivé  ankety spouštět
* koncem června bude k dispozici první verze (pilotní) nového rozhraní pro prezentaci výsledků
* bude-li nedostatečná, lze doplňkově využít i starou verzi prezentace (jako za semestr B181)

## K diskusi (na schůzi proděkanů)

* připomínky k dalšímu běhu ankety
* připomínky k plánu 2. fáze
* připomínky k celému procesu




