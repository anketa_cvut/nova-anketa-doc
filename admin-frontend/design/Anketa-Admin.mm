<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1570997388218" ID="ID_271278509" MODIFIED="1570997399824" TEXT="Anketa Admin">
<node CREATED="1570997405222" ID="ID_46299834" MODIFIED="1570999724196" POSITION="right" TEXT="p&#x159;ehled">
<node CREATED="1570997527904" ID="ID_1208957527" MODIFIED="1570997537683" TEXT="aktivn&#xed; ankety a stavy anket obecn&#x11b;"/>
<node CREATED="1570997538414" ID="ID_566057688" MODIFIED="1570997549449" TEXT="n&#xe1;hled &#x10d;asov&#xe9;ho pl&#xe1;nu"/>
<node CREATED="1570997550015" ID="ID_1071223690" MODIFIED="1570997565745" TEXT="posledn&#xed; koment&#xe1;&#x159;e &#x10d;i aktivity"/>
</node>
<node CREATED="1570997489334" ID="ID_1279806473" MODIFIED="1570998230910" POSITION="right" TEXT="ankety">
<node CREATED="1570997584300" ID="ID_414283485" MODIFIED="1570997624602" TEXT="nastaven&#xed; &#x10d;asov&#xe9;ho pl&#xe1;nu"/>
<node CREATED="1570997637128" ID="ID_1841444501" MODIFIED="1570997644746" TEXT="stavy ankety a jejich zm&#x11b;na"/>
<node CREATED="1570997648126" ID="ID_1505513538" MODIFIED="1570999778197" TEXT="spr&#xe1;va obsahu">
<node CREATED="1570997663398" ID="ID_1245313090" MODIFIED="1570997666418" TEXT="p&#x159;edm&#x11b;ty"/>
<node CREATED="1570997667230" ID="ID_1658634785" MODIFIED="1570997671939" TEXT="vyu&#x10d;uj&#xed;c&#xed;c&#xed;"/>
<node CREATED="1570997672702" ID="ID_702426426" MODIFIED="1570997675355" TEXT="studenti"/>
</node>
<node CREATED="1570997694717" ID="ID_488602578" MODIFIED="1570997705667" TEXT="n&#xe1;hled ankety (testovac&#xed; vypln&#x11b;n&#xed;)"/>
<node CREATED="1570999876478" ID="ID_880547110" MODIFIED="1570999880665" TEXT="kopie ankety"/>
<node CREATED="1571001874940" ID="ID_596429683" MODIFIED="1571001908024" TEXT="p&#x159;&#xed;prava v&#xfd;sledk&#x16f;">
<node CREATED="1571001911285" ID="ID_15069055" MODIFIED="1571001920464" TEXT="iniciace materializovan&#xfd;ch pohled&#x16f;"/>
<node CREATED="1571001921100" ID="ID_556326585" MODIFIED="1571001927960" TEXT="refresh materializovan&#xfd;ch pohled&#x16f;"/>
</node>
<node CREATED="1570997878656" ID="ID_644511768" MODIFIED="1570997882260" TEXT="statick&#xfd; export"/>
</node>
<node CREATED="1570997497621" ID="ID_288203859" MODIFIED="1570999726947" POSITION="right" TEXT="ot&#xe1;zky">
<node CREATED="1570997727503" ID="ID_490879590" MODIFIED="1570997732611" TEXT="spr&#xe1;va ot&#xe1;zek"/>
<node CREATED="1570997733351" ID="ID_1389851860" MODIFIED="1570997743267" TEXT="sestaven&#xed; ot&#xe1;zek k anket&#xe1;m"/>
</node>
<node CREATED="1570997505798" ID="ID_372027857" MODIFIED="1570999728602" POSITION="right" TEXT="koment&#xe1;&#x159;e">
<node CREATED="1570997771095" ID="ID_134039212" MODIFIED="1570997779163" TEXT="n&#xe1;hledy v&#xfd;sledk&#x16f; (koment&#xe1;&#x159;&#x16f;)"/>
<node CREATED="1570997780719" ID="ID_1328708914" MODIFIED="1570997783667" TEXT="cenzura"/>
</node>
<node CREATED="1570997511294" ID="ID_1802498349" MODIFIED="1570999729852" POSITION="right" TEXT="p&#x159;&#xed;stupy">
<node CREATED="1570997793343" ID="ID_220706189" MODIFIED="1570997796803" TEXT="u&#x10d;itel&#x16f;"/>
<node CREATED="1570997797617" ID="ID_790785738" MODIFIED="1570997801532" TEXT="student&#x16f;"/>
<node CREATED="1570997802319" ID="ID_1094361398" MODIFIED="1570997807379" TEXT="externist&#x16f;"/>
</node>
<node CREATED="1570997835680" ID="ID_155942578" MODIFIED="1570997847820" POSITION="right" TEXT="hlavn&#xed; spr&#xe1;vce">
<node CREATED="1570997912665" ID="ID_1509331191" MODIFIED="1570999733012" TEXT="&#x10d;&#xed;seln&#xed;ky - n&#xe1;hled + refresh?">
<node CREATED="1570997933105" ID="ID_682169850" MODIFIED="1570997969701" TEXT="p&#x159;edm&#x11b;ty"/>
<node CREATED="1570997957241" ID="ID_117792832" MODIFIED="1570997974589" TEXT="u&#x10d;itel&#xe9;"/>
<node CREATED="1570997975231" ID="ID_913526881" MODIFIED="1570997977861" TEXT="paralelky"/>
<node CREATED="1570997978705" ID="ID_1919130065" MODIFIED="1570997983733" TEXT="v&#xfd;uka"/>
<node CREATED="1570997986577" ID="ID_1917234933" MODIFIED="1570997989741" TEXT="studenti"/>
<node CREATED="1570997990241" ID="ID_210929747" MODIFIED="1570997994730" TEXT="zapsan&#xe9; p&#x159;edm&#x11b;ty"/>
</node>
<node CREATED="1570997943025" ID="ID_426535351" MODIFIED="1570998090965" TEXT="p&#x159;&#xed;stupy">
<node CREATED="1570997807919" ID="ID_1395995775" MODIFIED="1570997811580" TEXT="spr&#xe1;vc&#x16f;"/>
</node>
<node CREATED="1570999796261" ID="ID_1947656316" MODIFIED="1570999809785" TEXT="po&#x10d;&#xe1;te&#x10d;n&#xed; iniciace anket">
<node CREATED="1570999813869" ID="ID_1871984027" MODIFIED="1570999813869" TEXT=""/>
</node>
</node>
<node CREATED="1570999900118" ID="ID_1576263615" MODIFIED="1570999911570" POSITION="left" TEXT="obecn&#xe9; principy">
<node CREATED="1571000375067" ID="ID_682895918" MODIFIED="1571000497026">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      bude hodn&#283; anket (ka&#382;d&#225; fakulta novou za ka&#382;d&#253; semestr)
    </p>
    <ul>
      <li>
        na v&#353;ech str&#225;nk&#225;ch d&#225;t k dispozici filtr na semestr
      </li>
      <li>
        pou&#382;&#237;vat nastaven&#237; na defaultn&#237; semestr (implicitn&#283;)
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node CREATED="1571000501349" ID="ID_526421533" MODIFIED="1571001608738" POSITION="left" TEXT="opakuj&#xed;c&#xed; se &#x10d;innosti ">
<node CREATED="1571000516397" ID="ID_1231390295" MODIFIED="1571001842539">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      idea je, &#382;e hlavn&#237; admin p&#345;ednastav&#237; ankety (po&#269;&#225;te&#269;n&#237; iniciace ankety) a fakultn&#237; admini si pak budou nastaven&#237; prohl&#237;&#382;et a budou moci n&#283;co p&#345;idat nebo ubrat
    </p>
    <ul>
      <li>
        hlavn&#237; spr&#225;vce - po&#269;&#225;te&#269;n&#237; iniciace anket - p&#345;edm&#283;ty, vyu&#269;uj&#237;c&#237;, studenti - bude provedena spu&#353;t&#283;n&#237;m SQL skript&#367;
      </li>
      <li>
        ankety - spr&#225;va obsahu - tam si to budou fakultn&#237; spr&#225;vci prohl&#237;&#382;et a dola&#271;ovat
      </li>
      <li>
        hlavn&#237; spr&#225;vce&#160;ud&#283;len&#237; p&#345;&#237;stup&#367; do ankety (u&#269;itel&#233;, studenti) pomoc&#237; SQL skript&#367;
      </li>
      <li>
        p&#345;&#237;stupy - (u&#269;etel&#233;, studenti) - prohl&#237;&#382;en&#237; a dola&#271;ov&#225;n&#237;
      </li>
      <li>
        p&#345;&#237;stupy - externist&#233; - bude pln&#283; ve spr&#225;v&#283; fakult
      </li>
    </ul>
    <p>
      netrv&#225;m na tom, &#382;e to bude &#250;pln&#283; takto odd&#283;len&#233;, ale je t&#345;eba ohl&#237;dat, aby fakultn&#237; spr&#225;vce nepou&#353;t&#283;l n&#283;jak&#233; skripty v&#237;cekr&#225;t (v&#283;t&#353;inou by mu to stejn&#283; ne&#353;lo kv&#367;li PK)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</map>
