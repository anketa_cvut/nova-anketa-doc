[//]: # Nová anketa ČVUT

## Hlavní cíle

* Přitáhnout mlčící většinu
* Stejné možnosti vyjádření a sběru přidružených informací
* Pružnější a přehlednější zobrazování výsledků
* Možnost "nepředmětových" anket

## Přitáhnout mlčící většinu
* Atraktivní rozhraní
* Podpora rychlé cesty vyplnění
* Větší zpětná vazba

## Možnosti vyjádření a sběru dat
* Bodové hodnocení
* Textové komentáře
* Dynamické chování formuláře
* Anonymita/možnost podepsat textový komentář
* Sběr přidružených informací pro hlubší analýzy (ročník, studijní průměr, program, obor, role předmětu)

## Ukázka rozhraní

http://si-projekty-vyvoj.fit.cvut.cz/anketa/

## Zobrazování výsledků
* Zobrazovat mezivýsledky již v průběhu ankety
* Zobrazit příznak "shlédnuto vyučujícím" u textových komentářů
* Možnost pro vyučující (a nadřízené) přidat vyjádření k jednotlivých textovým komentářům
* Lepší agregace pro potřebu kateder/ústavů
* Lepší vyhledávání a možnost exportu dat
* Zobrazení trendů

## Stručný časový plán
* leden, únor - pilotní testování vyplňování (za zimní semestr)
* březen, duben - pilotní testování základního zobrazení výsledků
* červen - druhé pilotní testování (za letní semestr)
* léto - implementace sebraných požadavků na změny
* podzim - dokončení implementace sběru a zobrazování, rozhraní pro správce anket, dokumentace, zaškolení správců
* leden 2020 plné nasazení do provozu

## Nabídka pro fakulty
 Zúčastněte se pilotního testování, sběru požadavků na změny a jejich implementace.




