#! /bin/bash

filename=$1
extension="${filename##*.}"
prefix="${filename%.*}"
presfile=$(echo "pres-$prefix.tex")
texsource=$(echo "$prefix.tex")

title=$(grep '^\[//\]: # ' $filename | cut -c8-)

echo Processing: $prefix
echo Title is: $title
echo Creating: $presfile

cp preamble-presentation.tex $presfile
#echo '\title{'$title'}' >>$presfile 

cat >>$presfile <<EOF

\title{$title}

\begin{document}

  \begin{frame}
    \titlepage
  \end{frame}

  %becouse usage \documentclass[ignorre...] ignores \input (according to doc)

  \mode<all>

  \input{$texsource}

\end{document}
EOF

# cat $presfile




