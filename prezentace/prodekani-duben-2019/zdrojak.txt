je umisten na ../prodekani-leden-2019/work/les02...

podpora pro report o pilotu>

-- vyplnenost predmetovych anket

select o.short_name_cz "Fakulta ", f.quest_filled_total "Vyplněno lístků", f.quest_available_total "K dispozici lístků", (100*round(f.quest_filled_total/f.quest_available_total,2)) "Vyplněnost [%]"
from org_unit o join survey s on (o.org_unit_code=s.org_unit_org_unit_code)
         join v_course_survey_fillled_total f on (s.survey_key=f.survey_key and s.semester_semester_code=f.semester_code)
order by o.org_unit_code;

-- FEL stara anketa
--FEL  	4873	 16871  29%

-- anketa o anketa - pocet textovych komentaru  157
select count(*) from student_evaluation_answer where answer_text is not null;

-- pocet vyplnenych listku 539
select count(*) from student_evaluation_answer;

-- hodnoceni
select answer_value "Známka", count(*) "Počet známek" from student_evaluation_answer group by answer_value order by answer_value;

-- vsledna znamka 1.97
select round(avg(answer_value),2) as vysledna_znamka from student_evaluation_answer;

